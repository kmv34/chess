<?php


use Chess\ChessBoard;
use Chess\Units\UnitsFactory;
use PHPUnit\Framework\TestCase;


final class ChessBoardTest extends TestCase
{
    /**
     * @expectedException RuntimeException
     */
    public function testCanNotAddTwoFigureOnOneCell()
    {
        $chessBoard = ChessBoard::getInstance();
        $chessBoard->addPiece(UnitsFactory::create('pawn', 'w'), 'a', 1);
        $chessBoard->addPiece(UnitsFactory::create('bishop', 'b'), 'a', 1);
    }

    /**
     * @expectedException RuntimeException
     */
    public function testCanNotMoveOutsideBoard()
    {
        $chessBoard = ChessBoard::getInstance();
        $chessBoard->addPiece(UnitsFactory::create('pawn', 'w'), 'a', 1);
        $chessBoard->movePiece( 'a1', 'a9');
    }

}
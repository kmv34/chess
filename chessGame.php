#!/usr/bin/env php
<?php
require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Chess\Console\AddUnitCommand;
use Chess\Console\DeleteUnitCommand;
use Chess\Console\ExitCommand;
use Chess\Console\LoadGameCommand;
use Chess\Console\MenuCommand;
use Chess\Console\MoveUnitCommand;
use Chess\Console\NewGameCommand;
use Chess\Console\SaveGameCommand;
use Chess\Console\ShowChessBoardCommand;
use Chess\Events\ChessEventDispatcher;
use Chess\Events\UnitAddedListener;

$application = new Application();
$command = new MenuCommand();
$application->add($command);
$application->setDefaultCommand($command->getName());
$application->add(new ShowChessBoardCommand());
$application->add(new NewGameCommand());
$application->add(new AddUnitCommand());
$application->add(new MoveUnitCommand());
$application->add(new DeleteUnitCommand());
$application->add(new SaveGameCommand());
$application->add(new LoadGameCommand());
$application->add(new ExitCommand());


$dispatcher = ChessEventDispatcher::getInstance();
$listener = new UnitAddedListener();
$dispatcher->addListener('unit.added', array($listener, 'onUnitAddedAction'));


$application->run();
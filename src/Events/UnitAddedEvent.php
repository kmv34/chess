<?php


namespace Chess\Events;


use Symfony\Component\EventDispatcher\Event;


class UnitAddedEvent extends Event
{

    const NAME = 'unit.added';

    public function execute()
    {
        echo 'Unit was added to board' . PHP_EOL;
    }

}
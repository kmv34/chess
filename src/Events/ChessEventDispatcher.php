<?php


namespace Chess\Events;


use Symfony\Component\EventDispatcher\EventDispatcher;


class ChessEventDispatcher
{

    private static $instance;

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new EventDispatcher();
        }
        return self::$instance;
    }

    private function __clone()
    {
    }

    private function __construct()
    {
    }

    private function __wakeup()
    {
    }
}
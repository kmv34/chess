<?php
/**
 * Created by PhpStorm.
 * User: kobli
 * Date: 11.02.2018
 * Time: 11:21
 */

namespace Chess\Events;


use Symfony\Component\EventDispatcher\Event;

class UnitAddedListener
{

    public function onUnitAddedAction(Event $event)
    {
        $event->execute();
    }
}
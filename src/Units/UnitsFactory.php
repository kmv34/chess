<?php


namespace Chess\Units;


class UnitsFactory
{
    public static function create($type, $color)
    {
        switch ($type) {
            case 'bishop':
                return new Bishop($color);
            case 'pawn':
                return new Pawn($color);
        }
    }

    public static function getUnitsNames()
    {
        return ['king', 'queen', 'bishop', 'rock', 'knight', 'pawn'];
    }
}
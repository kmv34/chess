<?php


namespace Chess\Units;


class Pawn extends ChessUnit
{
    protected static $name = 'Bishop';
    protected $whiteSymbol = '♙';
    protected $blackSymbol = '♟';

    public function __construct($color)
    {
        $this->setColor($color);
    }

    /**
     * @param $from
     * @param $to
     * @return mixed
     */
    public function canMove($from, $to)
    {
        return true;
    }
}
<?php


namespace Chess\Units;


abstract class ChessUnit
{

    /**
     * @var string
     */
    protected static $name;

    /**
     * @var string
     */
    protected $color;

    protected $whiteSymbol;
    protected $blackSymbol;

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @throws \RuntimeException
     */
    public function setColor(string $color)
    {
        if ($color !== 'w' && $color !== 'b') {
            throw new \RuntimeException('Wrong color (w/b)');
        }
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getSymbol()
    {
        if ($this->color === 'w') {
            return $this->blackSymbol;
        }
        if ($this->color === 'b') {
            return $this->whiteSymbol;
        }
        return '?';
    }


    /**
     * @return string
     */
    public static function getName(): string
    {
        return static::$name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        static::$name = $name;
    }

    /**
     * @param $from
     * @param $to
     * @return mixed
     */
    abstract public function canMove($from, $to);
}
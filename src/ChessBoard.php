<?php


namespace Chess;


use Chess\Events\ChessEventDispatcher;
use Chess\Events\UnitAddedEvent;
use Chess\Units\ChessUnit;
use Symfony\Component\EventDispatcher\EventDispatcher;

class ChessBoard
{
    /**
     * @var null
     */
    private static $instance = null;
    /**
     * @var array
     */
    protected $pieces = [];
    /**
     * @var boolean
     */
    protected $status = false;
    const HORIZONTAL = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
    const VERTICAL = [1, 2, 3, 4, 5, 6, 7, 8];

    protected $dispatcher;

    /**
     * @return ChessBoard
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return array
     */
    public function getPieces()
    {
        return $this->pieces;
    }

    /**
     * @param array $pieces
     */
    public function setPieces(array $pieces)
    {
        $this->pieces = $pieces;
    }

    public function addPiece(ChessUnit $unit, $horizontal, $vertical)
    {
        $coords = $horizontal . $vertical;
        if (array_key_exists($coords, $this->pieces)) {
            throw new \RuntimeException('Field is not empty');
        }
        $this->pieces[$coords] = $unit;
        $event = new UnitAddedEvent();
        $this->dispatcher->dispatch(UnitAddedEvent::NAME, $event);
    }

    public function deletePiece($key)
    {
        unset($this->pieces[$key]);
    }

    public function movePiece($current, $target)
    {
        if (!array_key_exists($current, $this->pieces)) {
            throw new \RuntimeException('Field is empty');
        }
        if (\in_array(mb_substr($target, 0, 1), self::HORIZONTAL, true) ||
            \in_array((int)mb_substr($target, 1), self::VERTICAL, true)
        ) {
            throw new \RuntimeException('Target field out of range');
        }
        if ($this->pieces[$current]->canMove($current, $target)) {
            $this->pieces[$target] = $this->pieces[$current];
            unset($this->pieces[$current]);
        }
    }

    private function __clone()
    {
    }

    private function __construct()
    {
        $this->dispatcher = ChessEventDispatcher::getInstance();
    }

    private function __wakeup()
    {
    }
}
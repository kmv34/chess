<?php


namespace Chess\Console;


use Chess\ChessBoard;
use Chess\Storage\StorageFactory;
use Symfony\Component\Console\{
    Input\InputInterface,
    Output\OutputInterface,
    Question\ChoiceQuestion
};


class SaveGameCommand extends BaseCommand
{

    protected function configure()
    {
        $this
            ->setName('game:save')
            ->setDescription('Save game');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->checkGameStatus()) {

            $helper = $this->getHelper('question');
            $question = new ChoiceQuestion(
                'Choose storage: ',
                StorageFactory::getStorageNames(),
                0
            );
            $question->setErrorMessage('This storage does not exists');
            $storage = StorageFactory::create($helper->ask($input, $output, $question));
            $chessBoard = ChessBoard::getInstance();
            $storage->save($chessBoard->getPieces());
            $output->writeln('<info>Game has successfully saved</info>');
            $this->executeCommand((new MenuCommand())->getName(), $output);
        } else {
            $this->gameNotStartError('Game does not start!', $output);
        }
    }
}
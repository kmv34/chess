<?php


namespace Chess\Console;


class DeleteUnitCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('game:delete')
            ->setDescription('Delete game');
    }
}
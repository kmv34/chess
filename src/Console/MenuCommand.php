<?php


namespace Chess\Console;


use Symfony\Component\Console\{
    Input\InputInterface,
    Output\OutputInterface,
    Question\ChoiceQuestion
};


class MenuCommand extends BaseCommand
{

    private static $questions = [
        'New game',
        'Show board',
        'Add Unit',
        'Move Unit',
        'Remove Unit',
        'Save game',
        'Load game',
        'Exit',
    ];

    protected function configure()
    {
        $this
            ->setName('chess:menu')
            ->setDescription('MenuCommand application');
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Choose your action:',
            self::$questions,
            0
        );
        $question->setErrorMessage('Wrong command');
        $commandInstance = $this->getExecutableCommand($helper->ask($input, $output, $question));
        $this->executeCommand($commandInstance->getName(), $output);
    }
    public static function getQuestions(): array
    {
        return self::$questions;
    }

    private function getExecutableCommand($command)
    {
        if (\in_array($command, self::$questions, true)) {
            switch ($command) {
                case 'New game':
                    $commandInstance = new NewGameCommand();
                    break;
                case 'Show board':
                    $commandInstance = new ShowChessBoardCommand();
                    break;
                case 'Add Unit':
                    $commandInstance = new AddUnitCommand();
                    break;
                case 'Move Unit':
                    $commandInstance = new MoveUnitCommand();
                    break;
                case 'Remove Unit':
                    $commandInstance = new DeleteUnitCommand();
                    break;
                case 'Load game':
                    $commandInstance = new LoadGameCommand();
                    break;
                case 'Save game':
                    $commandInstance = new SaveGameCommand();
                    break;
                case 'Exit':
                    $commandInstance = new ExitCommand();
                    break;
                default:
                    $commandInstance = new MenuCommand();
            }
            return $commandInstance;
        }
    }
}
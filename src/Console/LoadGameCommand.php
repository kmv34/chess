<?php


namespace Chess\Console;


use Chess\ChessBoard;
use Chess\Storage\StorageFactory;
use Symfony\Component\Console\{
    Input\InputInterface, Output\OutputInterface, Question\ChoiceQuestion
};


class LoadGameCommand extends BaseCommand
{

    protected function configure()
    {
        $this
            ->setName('game:load')
            ->setDescription('Load game');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Choose storage: ',
            StorageFactory::getStorageNames(),
            0
        );
        $question->setErrorMessage('This storage does not exists');
        $storage = StorageFactory::create($helper->ask($input, $output, $question));
        $chessBoard = ChessBoard::getInstance();
        $loaded = $storage->load();
        if ($loaded) {
            $chessBoard->setPieces($loaded);
            $chessBoard->setStatus(true);
            $output->writeln('<info>Game successfully loaded</info>');
        } else {
            $output->writeln('<info>Can not find the game</info>');
        }
        $this->executeCommand((new MenuCommand())->getName(), $output);
    }
}
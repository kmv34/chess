<?php


namespace Chess\Console;


use Chess\ChessBoard;
use Symfony\Component\Console\{
    Command\Command,
    Input\ArrayInput,
    Output\OutputInterface
};

class BaseCommand extends Command
{

    /**
     * @param $commandName
     * @param OutputInterface $output
     */
    public function executeCommand(string $commandName, OutputInterface $output)
    {
        $command = $this
            ->getApplication()
            ->find($commandName);
        $arguments = [];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $output);
    }

    /**
     * @return bool
     */
    public function checkGameStatus(): bool
    {
        return null !== ChessBoard::getInstance();
    }

    /**
     * @param $error
     * @param OutputInterface $output
     */
    public function gameNotStartError($error, OutputInterface $output)
    {
        $output->writeln('<error>Game Not Found!</error>');
        $output->writeln('<info>' . $error . '</info>');
        $this->executeCommand((new MenuCommand())->getName(), $output);
    }
}
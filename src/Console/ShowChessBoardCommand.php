<?php


namespace Chess\Console;


use Chess\ChessBoard;
use Symfony\Component\Console\{
    Helper\Table,
    Input\InputInterface,
    Output\OutputInterface
};


class ShowChessBoardCommand extends BaseCommand
{

    protected function configure()
    {
        $this
            ->setName('board:show')
            ->setDescription('Show chessboard')
            ->setHidden(true);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->checkGameStatus()) {
            $board = ChessBoard::getInstance();
            $pieces = $board->getPieces();

            $rows = [];
            $horizontal = ChessBoard::HORIZONTAL;
            $vertical = ChessBoard::VERTICAL;
            foreach ($horizontal as $horizontalKey => $horizontalValue) {
                foreach ($vertical as $verticalKey => $verticalValue) {
                    if ($horizontalKey === 0) {
                        $rows[$verticalKey][0] = $verticalKey + 1;
                    }
                    if (array_key_exists($horizontalValue . $verticalValue, $pieces)) {
                        $rows[$verticalKey][$horizontalKey + 1] = $pieces[$horizontalValue . $verticalValue]->getSymbol();
                    } else {
                        $rows[$verticalKey][$horizontalKey + 1] = "\u{0020}";
                    }
                }
            }
            array_unshift($horizontal, ' ');
            $table = new Table($output);
            $table
                ->setHeaders($horizontal)
                ->setRows($rows);
            $table->render();
            $this->executeCommand((new MenuCommand())->getName(), $output);
        } else {
            $this->gameNotStartError('Чтобы посмотреть доску начните новую игру или загрузите существующую', $output);
        }
    }
}
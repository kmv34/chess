<?php


namespace Chess\Console;


use Chess\ChessBoard;
use Chess\Units\UnitsFactory;
use Symfony\Component\Console\{
    Input\InputInterface, Output\OutputInterface, Question\Question
};


class AddUnitCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('unit:add')
            ->setDescription('Add unit to chessboard');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->checkGameStatus()) {
            $helper = $this->getHelper('question');
            $question = new Question('Enter command ' . PHP_EOL . 'example: "bishop b a1"' . PHP_EOL);

            $createCode = $helper->ask($input, $output, $question);

            list($type, $color, $position) = explode(' ', trim($createCode));

            if (!in_array($type, UnitsFactory::getUnitsNames(), true)) {
                $output->writeln('<error>Wrong unit type</error>');
                $this->executeCommand((new AddUnitCommand())->getName(), $output);
            }

            $horizontal = mb_substr($position, 0, 1);
            $vertical = mb_substr($position, 1, 1);

            if (!in_array($horizontal, ChessBoard::HORIZONTAL, true)) {
                $output->writeln('<error>Wrong horizontal coordinates</error>');
                $this->executeCommand((new AddUnitCommand())->getName(), $output);
            }

            if (!in_array((int)$vertical, ChessBoard::VERTICAL, true)) {
                $output->writeln('<error>Wrong vertical coordinates</error>');
                $this->executeCommand((new AddUnitCommand())->getName(), $output);
            }

            $chessBoard = ChessBoard::getInstance();
            try {
                $chessBoard->addPiece(UnitsFactory::create($type, $color), $horizontal, $vertical);
            } catch (\RuntimeException $e) {
                $output->writeln('<error>' . $e->getMessage() . '</error>');
            }
            $this->executeCommand((new MenuCommand())->getName(), $output);
        } else {
            $this->gameNotStartError('Start or load game for adding units', $output);
        }
    }

}
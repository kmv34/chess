<?php


namespace Chess\Console;


use Chess\ChessBoard;
use Symfony\Component\Console\{
    Input\InputInterface, Output\OutputInterface, Question\Question
};


class MoveUnitCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('unit:move')
            ->setDescription('Move unit on chessboard');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->checkGameStatus()) {
            $helper = $this->getHelper('question');
            $question = new Question('Enter coordinates' . PHP_EOL . 'example: "a1 f8"' . PHP_EOL);

            $coordinates = $helper->ask($input, $output, $question);

            list($from, $to) = explode(' ', trim($coordinates));

            $fromHorizontal = mb_substr($from, 0, 1);
            $fromVertical = mb_substr($from, 1, 1);

            if (!in_array($fromHorizontal, ChessBoard::HORIZONTAL, true)) {
                $output->writeln('<error>Wrong horizontal coordinates</error>');
            }

            if (!in_array((int)$fromVertical, ChessBoard::VERTICAL, true)) {
                $output->writeln('<error>Wrong vertical coordinates</error>');
            }

            $toHorizontal = mb_substr($to, 0, 1);
            $toVertical = mb_substr($to, 1, 1);

            if (!in_array($toHorizontal, ChessBoard::HORIZONTAL, true)) {
                $output->writeln('<error>Wrong horizontal coordinates</error>');
            }

            if (!in_array((int)$toVertical, ChessBoard::VERTICAL, true)) {
                $output->writeln('<error>Wrong vertical coordinates</error>');
            }

            $chessBoard = ChessBoard::getInstance();
            try {
                $chessBoard->movePiece($from, $to);
            } catch (\RuntimeException $e) {
                $output->writeln('<error>' . $e->getMessage() . '</error>');
            }
            $this->executeCommand((new MenuCommand())->getName(), $output);
        } else {
            $this->gameNotStartError('Start or load game for adding units', $output);
        }
    }
}
<?php

namespace Chess\Console;


use Chess\ChessBoard;
use Symfony\Component\Console\{
    Input\InputInterface,
    Output\OutputInterface
};


class NewGameCommand extends BaseCommand
{

    protected function configure()
    {
        $this
            ->setName('game:new')
            ->setDescription('MenuCommand new game');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $board = ChessBoard::getInstance();
        if ($board->isStatus()) {
            $board->setPieces([]);
        }
        $board->setStatus(true);
        $output->writeln('<info>New game has started!</info>');
        $this->executeCommand((new MenuCommand())->getName(), $output);
    }
}
<?php


namespace Chess\Console;


use Symfony\Component\Console\{
    Input\InputInterface,
    Output\OutputInterface
};


class ExitCommand extends BaseCommand
{

    protected function configure()
    {
        $this
            ->setName('exit')
            ->setDescription('Exit application');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Bye!');
    }
}
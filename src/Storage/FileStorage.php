<?php


namespace Chess\Storage;


class FileStorage implements StorageInterface
{
    protected static $title = 'File System';
    protected $path = __DIR__ . '/../../storage/data.txt';


    public function load()
    {
        if (file_exists($this->path)) {
            return unserialize(file_get_contents($this->path), ['allowed_classes' => true]);
        }
        return false;
    }


    public function save($data)
    {
        file_put_contents($this->path, serialize($data));
    }
}
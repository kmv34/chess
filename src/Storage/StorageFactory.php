<?php


namespace Chess\Storage;


class StorageFactory
{
    public static function create($type)
    {
        switch ($type) {
            case 'file':
                return new FileStorage();
            case 'redis':
                return new RedisStorage();
        }
    }

    public static function getStorageNames()
    {
        return ['file', 'redis'];
    }
}
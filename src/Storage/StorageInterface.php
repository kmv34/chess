<?php
/**
 * Created by PhpStorm.
 * User: kobli
 * Date: 09.02.2018
 * Time: 14:22
 */

namespace Chess\Storage;


interface StorageInterface
{
    public function save($data);
    public function load();
}
<?php


namespace Chess\Storage;


use Redis;


class RedisStorage implements StorageInterface
{
    protected static $title = 'Redis';
    protected $client;

    public function __construct()
    {
        $this->client = new Redis();
        $this->client->connect('127.0.0.1', 6379);
        $this->client->auth('password');
    }

    public function load()
    {
        return unserialize($this->client->get('chess'), ['allowed_classes' => false]);
    }

    public function save($data)
    {
        $this->client->set('chess', serialize($data));
    }
}